#!/usr/bin/env python3                         
# encoding: utf-8

# Importación de bibliotecas necesarias
import time
import rospy
from dynamixel_sdk import *
from dynamixel_sdk_examples.srv import *
from dynamixel_sdk_examples.msg import *

# Función para generar una lista de números en un rango específico
def generar_numeros(min_value, max_value, reversed=0):
    """
    Genera una lista de números en un rango específico.

    Parameters:
        min_value (int): Valor mínimo del rango.
        max_value (int): Valor máximo del rango.
        reversed (int, optional): Indica si se debe invertir la lista. Por defecto es 0 (no invertir).

    Returns:
        list: Lista de números en el rango especificado.
    """
    numeros = []
    for i in range(min_value, max_value):
        numeros.append(i)

    if reversed == 1:
        numeros.reverse()

    return numeros


def nodo(data, servoID, rate):
    """
    Controla el movimiento de un servomotor específico mediante la publicación de datos de posición.

    Parameters:
        data (list): Lista de valores que representan las posiciones a las que se moverá el servomotor.
        servoID (int): Identificador único del servomotor que se controlará.
        rate (int): Tasa de publicación de mensajes, que determina la velocidad del movimiento del servomotor.

    Returns:
        None
    """
    mensaje = data
    i=0
    id= servoID                        
    rospy.init_node('Enviar_datos_node')                       #Inicializamos nuestro nodo y le asignamos un nombre = nodo_publisher
    
    pub = rospy.Publisher('set_position', SetPosition, queue_size=10) 
    rate = rospy.Rate(rate)    

    while not rospy.is_shutdown():                          #Bucle While - hasta pulsar Ctrl-C
        print('Se esta enviado el dato',mensaje[i], 'en la id :', id)
        pub.publish( id, mensaje[i] ) 
        
        if i==(len(mensaje)-1):
            break
        i=i+1
        rate.sleep()


if __name__ == '__main__':
    try:
        # Configuración de las posiciones iniciales de los servomotores
        datos = generar_numeros(400, 513, 0)
        nodo(datos, servoID=1, rate=80)  # Servo #1 Posición Inicial.
        time.sleep(1)
        
        datos = generar_numeros(480, 515, 0)
        nodo(datos, servoID=2, rate=80)  # Servo #2 Posición Inicial.
        time.sleep(1)
        
        datos = generar_numeros(480, 513, 0)
        nodo(datos, servoID=3, rate=80)  # Servo #3 Posición Inicial.
        time.sleep(1)
        
        datos = generar_numeros(450, 513, 0)
        nodo(datos, servoID=4, rate=80)  # Servo #4 Posición Inicial.
        time.sleep(1)
        
        datos = generar_numeros(450, 513, 0)
        nodo(datos, servoID=5, rate=80)  # Servo #5 Posición Inicial.
        time.sleep(1)
        
        datos = generar_numeros(312, 513, 0)
        nodo(datos, servoID=6, rate=80)  # Servo #6 Posición Inicial.
        time.sleep(1)
        
        # Movimientos de los servomotores a nuevas posiciones
        datos = generar_numeros(385, 513, 1)
        nodo(datos, servoID=6, rate=80)  # Servo #6 
        time.sleep(1)
        
        datos = generar_numeros(302, 513, 1)
        nodo(datos, servoID=1, rate=80)  # Servo #1 
        time.sleep(1)
        
        datos = generar_numeros(515, 710, 0)
        nodo(datos, servoID=2, rate=80)  # Servo #2 
        time.sleep(1)
        
        datos = generar_numeros(513, 637, 0)
        nodo(datos, servoID=3, rate=80)  # Servo #3 
        time.sleep(1)
        
        datos = generar_numeros(250, 550, 1)
        nodo(datos, servoID=4, rate=80)  # Servo #4 
        time.sleep(1)
        
        datos = generar_numeros(637, 735, 0)
        nodo(datos, servoID=3, rate=80)  # Servo #3 
        time.sleep(1)
        
        datos = generar_numeros(385, 513, 0)
        nodo(datos, servoID=6, rate=80)  # Servo #6 
        time.sleep(1)
        
        datos = generar_numeros(513, 735, 1)
        nodo(datos, servoID=3, rate=80)  # Servo #3 
        time.sleep(1)
        
        datos = generar_numeros(515, 710, 1)
        nodo(datos, servoID=2, rate=80)  # Servo #2 
        time.sleep(1)
        
        datos = generar_numeros(302, 513, 0)
        nodo(datos, servoID=1, rate=80)  # Servo #1 
        time.sleep(1)
        
        datos = generar_numeros(515, 650, 0)
        nodo(datos, servoID=2, rate=80)  # Servo #2
        time.sleep(1)
        
        datos = generar_numeros(513, 600, 0)
        nodo(datos, servoID=3, rate=80)  # Servo #3 
        time.sleep(1)
        
        datos = generar_numeros(385, 513, 1)
        nodo(datos, servoID=6, rate=80)  # Servo #6 
        time.sleep(1)
        
        # Regreso a la posición inicial
        datos = generar_numeros(515, 650, 1)
        nodo(datos, servoID=2, rate=80)  # Servo #2
        time.sleep(1)
        
        datos = generar_numeros(513, 600, 1)
        nodo(datos, servoID=3, rate=80)  # Servo #3 
        time.sleep(1)
        
        datos = generar_numeros(250, 520, 0)
        nodo(datos, servoID=4, rate=80)  # Servo #4
        time.sleep(1)

    except rospy.ROSInterruptException:  # Captura de excepciones para terminar la ejecución del nodo al presionar Ctrl-C
        pass
